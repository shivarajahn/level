## inCyyte User Interface

Welcome to the inCyyte User Interface git respository. This repository contains all of the stylesheets, fonts and images that connect to the inCyyte front end HTML.

This is how to setup your machine for development 24.

**Install dependencies on your machine**

* Download and install [git](http://git-scm.com/download/)
* Download and install [Node.js](https://nodejs.org/download) on your local machine
* Open the Node command line utility

Check that everything is installed correctly

`git --version`

`node --version`

`npm --version`

Ensure that you have all of these dependencies.

**Get The Repository**

`git clone git@bitbucket.org:dapple/incyyte.git`

**Install project dependencies**

`npm install --save-dev`

`npm install -g grunt-cli`

Check that grunt is installed correctly with the following command:

`grunt --version`

You can now run the grunt command to compile all less files into CSS files.

`grunt`

You can also run a deamon to compile all less files into CSS files when you save them. How cool is that?!

`grunt watch`